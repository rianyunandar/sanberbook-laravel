<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">


<head>
    <title>SanberBook-Form SignUP</title>
</head>

<body>

    <h1>Buat Akun Baru</h1>
    <h3>SignUp Form</h3>

    <form action="/welcome" method="POST">
        @csrf
         <label for="fname">First name:</label><br>
 
        <input type="text" id="fname" name="fname" placeholder="Input your first name"><br>
        <label for="lname">Last name:</label><br>
        <input type="text" id="lname" name="lname" placeholder="Input your last name"><br>
        <br><label for="fgender">gender:</label><br>
        <input type="radio" id="male" name="gender" value="male">
        <label for="male">male</label><br>
        <input type="radio" id="female" name="gender" value="female">
        <label for="female">female</label><br>
        <input type="radio" id="other" name="gender" value="other">
        <label for="other">other</label><br>
        <br>
        <label for="nasionality">Nasionality</label><br><br>
        <select name="nasionality" id="nasionality">
            <option value="indonesian">Indonesian</option>
            <option value="malaysian">Malaysian</option>
            <option value="singapore">Singapore</option>
            <option value="other">Other</option>
        </select><br><br>
        <label for="spoken">spoken</label><br>
        <input type="checkbox" id="spoken1" name="spoken1" value="Bahasa">
        <label for="spoken1">Bahasa Indonesia</label><br>
        <input type="checkbox" id="spoken2" name="spoken2" value="English">
        <label for="spoken2">English</label><br>
        <input type="checkbox" id="spoken3" name="spoken3" value="Other">
        <label for="spoken3">Other</label><br><br>

        <label for="nasionality">Nasionality</label><br>
        <textarea id="bio" name="bio" rows="4" cols="50">
            </textarea>
        <br>
        <input type="submit" value="Submit">
    </form>
</body>

</html>